assert = require 'assert'
sinon = require 'sinon'
Router = require '../lib/router'

describe 'Router', () ->

  describe '#ensureSlash()', () ->
    it 'should return path with slashes', () ->
      assert.equal Router.prototype.ensureSlash('foo'), '/foo/'

    it 'should not add any additional slashes', () ->
      assert.equal Router.prototype.ensureSlash('/foo/'), '/foo/'

  describe '#getAll()', () ->
    # Set up mock model
    MockModel = {}
    MockModel.find = sinon.spy()
    req = {}

    it "should call model's #find method", () ->
      router = new Router MockModel, 'path'
      router.getAll(req, 'foo')
      assert MockModel.find.calledWith {}, 'foo'

  describe '#getOne()', () ->
    # Set up mocks
    MockModel = {}
    MockModel.findOne = sinon.spy()
    req = 
      params:
        id: 'bar'

    it "should call model's #foneOne method", () ->
      router = new Router MockModel, 'path'
      router.getOne(req, 'foo')
      assert MockModel.findOne.calledWith {'_id': 'bar'}, 'foo'

    it "should use whatever ID field we tell it to use", () ->
      router = new Router MockModel, 'path', 'bar'
      router.getOne(req, 'foo')
      assert MockModel.findOne.calledWith {'bar': 'bar'}, 'foo'

  describe '#getDocument()', () ->
    # Set up mocks
    MockModel = 
      err: null
      doc: {}
      findOne: (args, cb) ->
        @args = args
        cb(@err, @doc)

    req =
      params:
        id: 'fooId'

    res = 
      json: sinon.spy()

    it "should call #getOne()", () ->
      next = sinon.spy()  
      router = new Router MockModel, 'path'
      router.getDocument req, res, next
      assert next.called
      assert.equal req.doc, MockModel.doc

    it "should pass error to callback, if there is error", () ->
      next = sinon.spy()
      MockModel.err = 'Boo!'
      req.doc = null
      router = new Router MockModel, 'path'
      router.getDocument req, res, next
      assert next.calledWith 'Boo!'
      assert.equal req.doc, null

    it "should return 404 if no document", () ->
      next = sinon.spy()
      MockModel.err = null
      MockModel.doc = null
      req.doc = null
      router = new Router MockModel, 'path'
      router.getDocument req, res, next
      assert not next.called
      assert res.json.calledWith 404, {err: 'Not found'}

  describe "#getListMiddleware()", () ->

    MockModel = {}
    middleware = [1, 2, 3]

    it "should return default middleware", () ->
      router = new Router MockModel, 'path'
      assert.deepEqual router.getListMiddleware(), []

      router = new Router MockModel, 'path', null, middleware
      assert.deepEqual router.getListMiddleware(), middleware

  describe "#getCreateMiddleware()", () ->

    MockModel = {}
    middleware = [1, 2, 3]

    it "should return default middleware", () ->
      router = new Router MockModel, 'path'
      assert.deepEqual router.getCreateMiddleware(), []

      router = new Router MockModel, 'path', null, middleware
      assert.deepEqual router.getCreateMiddleware(), middleware

  describe "#getShowMiddleware()", () ->

    MockModel = {}
    middleware = [1, 2, 3]

    it "should return default middleware plus one more", () ->
      router = new Router MockModel, 'path'
      # We test only the length here, because of the complex way in which
      # the getDocument middleware is bound to Router object by CoffeeScript
      # compiler.
      assert.equal router.getShowMiddleware().length, 1

      router = new Router MockModel, 'path', null, middleware
      assert.equal router.getShowMiddleware().length, middleware.length + 1

  describe "#getUpdateMiddleware()", () ->

    MockModel = {}
    middleware = [1, 2, 3]

    it "should return default middleware plus one more", () ->
      router = new Router MockModel, 'path'
      assert.equal router.getUpdateMiddleware().length, 1

      router = new Router MockModel, 'path', null, middleware
      assert.equal router.getUpdateMiddleware().length, middleware.length + 1

  describe "#getDeleteMiddleware()", () ->

    MockModel = {}
    middleware = [1, 2, 3]

    it "should return default middleware plus one more", () ->
      router = new Router MockModel, 'path'
      assert.equal router.getDeleteMiddleware().length, 1

      router = new Router MockModel, 'path', null, middleware
      assert.equal router.getDeleteMiddleware().length, middleware.length + 1