###!
Router
======

@author Branko Vukelic <branko@brankovukelic.com>
@license MIT
@version 0.0.3
###

class Router
  ###
  Router(Model, baseUrl, [idField, middleware])
  ---------------------------------------------

  @param Model {Function} Mongoose model constructor
  @param baseUrl {String} Base URL of the routes
  @param idField {String} Model key to use as ID in routes
  @param middlewares {Array} Array of middleware functions to use on routes
  ###
  constructor: (@Model, @baseUrl, @idField='_id', @middleware=[]) ->
    @baseUrl = @ensureSlash @baseUrl
    @baseItemUrl = @baseUrl + ':id'

  NOT_FOUND: 404
  BAD_REQUEST: 400
  OK: 200

  ###
  #ensureSlash(path)
  ----------------------------------

  Make sure the path has both leading and trailing slash.

  @param path {String} Path
  ###
  ensureSlash: (path) ->
    if path[0] isnt '/'
      path = "/#{path}"
    if path[path.length - 1] isnt '/'
      path = "#{path}/"
    path

  ###
  #getAll(req, cb)
  ---------------------------

  Fetch all objects in the list route handler and call a callback.

  @param req {Object} Request object
  @param cb {Function} Callback function
  ###
  getAll: (req, cb) ->
    @Model.find {}, cb

  ###
  #getOne(req, id, cb)
  ---------------------------

  Fetch all objects in the list route handler and call a callback.

  @param req {Object} Request object
  @param cb {Function} Callback function
  ###
  getOne: (req, cb) ->
    # Build query object manually since there's no better way
    query = {}
    query[@idField] = req.params.id
    @Model.findOne query, cb

  ###
  #processError(err)
  ----------------------------------

  Process the error object returned from Mongoose.

  This method just returns the error object that is passed to it by default. 
  It exists so developers can overload it and customize its behavior.

  @param err {Object} Error object returned by Mongoose
  ###
  processError: (err) ->
    err

  ###
  #getDocument(req, res, next)
  --------------------------------------------

  This is amiddleware that fetches a single document from the database and
  attaches it to request object as `doc` attribute.

  By default, this method will not handle any errors, and it will return a 
  HTTP 404 if the document with specified ID doesn't exist.

  @param req {Object} Request object
  @param res {Object} Response object
  @param next {Function} Callback function
  ###
  getDocument: (req, res, next) =>
    @getOne req, (err, doc) =>
      return next(err) if err
      return res.json @NOT_FOUND, {err: 'Not found'} if not doc
      req.doc = doc
      next()

  ###
  #getListMiddleware()
  ------------------------------------

  Returns a set of middleware for the list route.

  @return {Array} Middleware for the route
  ###
  getListMiddleware: () ->
    @middleware

  ###
  #getCreateMiddleware()
  --------------------------------------

  Returns a set of middleware for the create route.

  @return {Array} Middleware for the route
  ###
  getCreateMiddleware: () ->
    @middleware

  ###
  #getShowMiddleware()
  ------------------------------------

  Returns a set of middleware for the show route.

  @return {Array} Middleware for the route
  ###
  getShowMiddleware: () ->
    @middleware.concat @getDocument

  ###
  #getUpdateMiddleware()
  --------------------------------------

  Returns a set of middleware for the update route.

  @return {Array} Middleware for the route
  ###
  getUpdateMiddleware: () ->
    @middleware.concat @getDocument

  ###
  #etDeleteMiddleware()
  --------------------------------------

  Returns a set of middleware for the delete route.

  @return {Array} Middleware for the route
  ###
  getDeleteMiddleware: () ->
    @middleware.concat @getDocument

  ###
  #list(req, res, next)
  -------------------------------------

  Handler for the list route:

    GET /collection/

  @param req {Object} Request object
  @param res {Object} Response object
  @param next {Function} Callback function
  ###
  list: (req, res, next) =>
    @getAll req, (err, docs=[]) =>
      return next(err) if err
      res.json @OK, docs

  ###
  #create(req, res, next)
  ---------------------------------------

  Handler for the create route:

    POST /collection/

  @param req {Object} Request object
  @param res {Object} Response object
  @param next {Function} Callback function
  ###
  create: (req, res, next) =>
    @Model.create req.body, (err, doc) =>
      return res.json @BAD_REQUEST, {err: @processError err} if err
      res.json @OK, doc

  ###
  #show(req, res, next)
  -------------------------------------

  Handler for the show route:

    GET /collection/:id

  @param req {Object} Request object
  @param res {Object} Response object
  @param next {Function} Callback function
  ###
  show: (req, res, next) =>
    res.json @OK, req.doc

  ###
  #update(req, res, next)
  ---------------------------------------

  Handler for the update route:

    PUT /collection/:id

  @param req {Object} Request object
  @param res {Object} Response object
  @param next {Function} Callback function
  ###
  update: (req, res, next) =>
    req.doc.set req.body
    req.doc.save (err, doc) =>
      if err
        return res.json @BAD_REQUEST, {err: @processError err}
      res.json @OK, req.doc

  ###
  #del(req, res, next)
  ------------------------------------

  Handler for the delete route:

    DELETE /collection/:id

  @param req {Object} Request object
  @param res {Object} Response object
  @param next {Function} Callback function
  ###
  del: (req, res, next) =>
    req.doc.remove (err) =>
      return next(err) if err
      res.json @OK, {id: req.params.id}

  ###
  #route(app)
  ---------------------------

  Set up routing for a given app.

  @param app {Object} Express application instance
  @return {Object} Same application instance returned for chaining
  ###
  route: (app) ->
    # GET /base/ - list
    app.get @baseUrl, @getListMiddleware(), @list

    # POST /base/ - create
    app.post @baseUrl, @getCreateMiddleware(), @create

    # GET /base/:id - show
    app.get @baseItemUrl, @getShowMiddleware(), @show

    # PUT /base/:id - update
    app.put @baseItemUrl, @getUpdateMiddleware(), @update

    # DELETE /base/:id - delete
    app.del @baseItemUrl, @getDeleteMiddleware(), @del

    app


module.exports = Router