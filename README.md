node-restrouter
===============

Simple ExpressJS helper for creating REST interface for Mongoose models.

This library is intended to work with ExpressJS framework.

Router is a highly customizable helper class for generating ExpressJS routes
that provide a REST interface for Mongoose models. Its aim is to be 
compatible with BackboneJS, but it doesn't do anything Backbone-specific.

node-restrouter is written in CoffeeScript. JavaScript version of the code is provided for convenience.

Installation
------------

Install with NPM:

    npm install node-restrouter

You can also simply grab the `router.js` file from the `lib` directory and
drop it in wherever you want.

Dependencies
------------

There are none. In theory, the Router calss will work with any model object 
that supports Mongoose-compatible API, and any framework that supports 
Express-like API.

Changes
-------

This is a list of backwards-incompatible changes.

 + 0.0.2 -> 0.0.3: `#getAll()` and `#getOne()` now take req as first argument.
   If you have overloaded either of these methods, please update the 
   signatures.

Running tests
-------------

To run tests, you first need to compile the code. Install development 
dependencies by going to source directory and running

    npm install

Compile the code:

    volo compile

Next run the tests with:

    mocha test/*.js  # use backslash instead of forward slash on Windows

Basic usage
-----------

To create a new router object, simply call the constructor passing it the 
model constructor and the base URL at which it shoudl be routed. Note that
there is no support for nested resources at this moment.

Here's a simple example:

    Book = require('./models/book'); # model
    Router = require('node-restrouter').Router;
  
    bookRouter = Router(Book, 'books'); # slashes are automatically appended

Now, call the router's `route` method, and pass it the express app instance:

    bookRouter.route(app);

This should create a following set of routes:

    GET /books/         # get all books
    POST /books/        # create a new book
    GET /books/:id      # get one book with specified id
    PUT /books/:id      # update a book with specified id
    DELETE /books/:id   # delete a book with specified id

Custom id field
---------------

The `:id` parameter can be any key on the model. By default, it's the `_id` 
key. You can override this by passing the name of the field you want to use as 
the third parameter to the constructor. Not that this doesn't change the 
route signatures.

    bookRouter = Router(Book, 'books', 'slug');

The above router uses the 'slug' field as the `:id` parameter.

Middleware
----------

You can pass in an array of middleware functions to the `Router` constructor.
The middlewares are then used on all routes. See the section on advanced 
customizaton for information on how to specify different middleware per 
route.

    bookRouter = Router(Book, 'books', 'slug', [authRequired]);

Advanced customization
----------------------

The Router constructor is organized in a way that allows you to easily change 
its behavior either partially or completely. This is accomplished by handling 
different aspects of route handling into separate methods that you can 
overload in your instances.

Here is a simple example that changes the way lists of documents are returned.

    bookRouter = Router(Book, 'books')
    bookRouter.getAll = function(cb) {
      this.Model.where('year').gt(2008).exec(cb);
    };

Look in source code for other ways to overload the methods and customize the 
router.

Reporting bugs
--------------

Please use the [BitBucket issue tracker](https://bitbucket.org/brankovukelic/node-restrouter/issues)
to report issues and feature requests.